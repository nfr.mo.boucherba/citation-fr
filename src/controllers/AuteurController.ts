import { ALL } from "dns";
import { Request, Response } from "express-serve-static-core";

export default class AuteurController
{
    /**
     * Affiche la liste des auteur
     * @param req 
     * @param res 
     */
    static Auteur(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const auteurall = db.prepare('SELECT * FROM auteur ' ).all();

        const citation = db.prepare('SELECT * FROM citation').all();

        res.render('pages/auteur', {
            title: 'auteur',
            auteurall: auteurall,

            citation: citation
        });
    }
    /**
     * Affiche la liste des auteur
     * @param req 
     * @param res 
     */
     static auteurview(req: Request, res: Response)
     {
         const db = req.app.locals.db;

         const auteurget = db.prepare('SELECT * FROM auteur WHERE id = ?' ).get(req.params.id);

         const citation = db.prepare('SELECT * FROM citation WHERE auteur_id = ?').all(req.params.id);
 
         res.render('pages/auteur-view', {
             title: 'auteur',
             citation: citation,
             auteurget: auteurget
         });
     }

    /**
     * Affiche le formulaire de creation d'auteur
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void
    {
        res.render('pages/auteur-create');
    }

    /**
     * Recupere le formulaire et insere l'auteur en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO auteur ("username", "password") VALUES (?, ?)').run(req.body.username, req.body.password);

        AuteurController.Auteur(req, res);
    }

    /**
     * Affiche 1 auteur
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const auteur = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);

        res.render('pages/auteur-view', {
            auteur: auteur
        });
    }



    /**
     * Affiche le formulaire pour modifier un auteur
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const auteur = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);

        res.render('pages/auteur-update', {
            auteur: auteur
        });
    }

    /**
     * Recupere le formulaire de l'auteur modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE auteur SET username = ?, password = ? WHERE id = ?').run(req.body.username, req.body.password, req.params.id);

        AuteurController.Auteur(req, res);
    }

    /**
     * Supprime un auteur
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM auteur WHERE id = ?').run(req.params.id);

        AuteurController.Auteur(req, res);
    }
}