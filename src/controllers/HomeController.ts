import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const auteurall = db.prepare('SELECT * FROM auteur ' ).all();

        const citation = db.prepare('SELECT * FROM citation ').all();
        const lastcitation = citation.length
        const truelastcitation = citation[lastcitation-1].content
        const fulllastcitation = citation[lastcitation-1]

        res.render('pages/index', {
            title: 'Homepage',
            auteurall: auteurall,
            fulllastcitation: fulllastcitation,
            citation: citation
        });
        
        
        
        
        
    }

}