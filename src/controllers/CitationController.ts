import { Request, Response } from "express-serve-static-core";

export default class CitationController
{
    /**
     * Affiche la liste des citation
     * @param req 
     * @param res 
     */
    static Citation(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const citation = db.prepare('SELECT * FROM citation').all();
        const auteur = db.prepare('SELECT * FROM auteur').all();


        res.render('pages/citation', {
            title: 'Bienvenue à la page de citation',
            citation: citation,
            auteur: auteur
        });
    }

    /**
     * Affiche le formulaire de creation de citation
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void
    {
        res.render('pages/citation-create');
    }

    /**
     * Recupere le formulaire et insere une citation en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void
    {
        // ah c'est void c'est pour ça off
        const db = req.app.locals.db;

        let citation = db.prepare('SELECT * FROM citation WHERE id > 0').get();
        let auteur = db.prepare('SELECT * FROM auteur').all();
        let auteurid = db.prepare('select id FROM auteur').all();

        db.prepare('INSERT INTO citation ("content","auteur_id") VALUES (?,?)').run(req.body.content, req.body.auteur_id);


        CitationController.Citation(req, res);
    }

    /**
     * Affiche 1 citation
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const citation = db.prepare('SELECT * FROM citation WHERE id = ?').get(req.params.id);

        const auteurdecitation = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);
        
        res.render('pages/citation-all', {
            citation: citation,

            auteurdecitation: auteurdecitation
        });
    }

    /**
     * Affiche le formulaire pour modifier un citation
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const citation = db.prepare('SELECT * FROM citation WHERE id = ?').get(req.params.id);

        const auteurdecitation = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);


        res.render('pages/citation-update', {
            citation: citation,

            auteurdecitation: auteurdecitation
        });
    }

    /**
     * Recupere le formulaire de l'auteur modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE citation SET content = ?,auteur_id = ? WHERE id = ?').run(req.body.content, req.body.auteur_id, req.params.id);

        CitationController.Citation(req, res);
    }

    /**
     * Supprime un auteur
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response)
    {
        const db = req.app.locals.db;
        

        db.prepare('DELETE FROM citation WHERE id = ?').run(req.params.id);

        CitationController.Citation(req, res);
    }
}