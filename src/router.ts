import { Application } from "express";
import AuteurController from "./controllers/AuteurController";
import HomeController from "./controllers/HomeController";
import CitationController from "./controllers/CitationController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });
    //auteur

    app.get('/auteur-all', (req, res) =>
    {
        AuteurController.Auteur(req, res)
    });

    app.get('/auteur-create', (req, res) =>
    {
        AuteurController.showForm(req, res)
    });

    app.post('/auteur-create', (req, res) =>
    {
        AuteurController.create(req, res)
    });

    app.get('/auteur-view/:id', (req, res) =>
    {
        AuteurController.auteurview(req, res)
    });

    app.get('/auteur-update/:id', (req, res) =>
    {
        AuteurController.showFormUpdate(req, res)
    });

    app.post('/auteur-update/:id', (req, res) =>
    {
        AuteurController.update(req, res)
    });

    app.get('/auteur-delete/:id', (req, res) =>
    {
        AuteurController.delete(req, res)
    });

    //fin auteur

    // citation
    app.get('/citation-all', (req, res) =>
    {
        CitationController.Citation(req, res)
    });

    app.get('/citation-create', (req, res) =>
    {
        CitationController.showForm(req, res)
    });

    app.post('/citation-create', (req, res) =>
    {
        CitationController.create(req, res)
    });

    app.get('/citation-read/:id', (req, res) =>
    {
        CitationController.read(req, res)
    });

    app.get('/citation-update/:id', (req, res) =>
    {
        CitationController.showFormUpdate(req, res)
    });

    app.post('/citation-update/:id', (req, res) =>
    {
        CitationController.update(req, res)
    });

    app.get('/citation-delete/:id', (req, res) =>
    {
        CitationController.delete(req, res)
    });
}
