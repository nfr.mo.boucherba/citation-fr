-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Mohamed
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 11:52
-- Created:       2022-02-21 11:43
PRAGMA foreign_keys = OFF;

-- Schema: mydb
ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "mydb"."auteur"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL
);
CREATE TABLE "mydb"."citation"(
  "id" INTEGER PRIMARY KEY NOT NULL,
  "content" VARCHAR(255) NOT NULL,
  "auteur_id" INTEGER NOT NULL,
  CONSTRAINT "fk_citation_auteur1"
    FOREIGN KEY("auteur_id")
    REFERENCES "auteur"("id")
);
CREATE INDEX "mydb"."citation.fk_citation_auteur1_idx" ON "citation" ("auteur_id");
CREATE TABLE "mydb"."auteur_has_citation"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "auteur_id" INTEGER NOT NULL,
  "citation_id" INTEGER NOT NULL,
  CONSTRAINT "fk_auteur_has_citation_auteur"
    FOREIGN KEY("auteur_id")
    REFERENCES "auteur"("id"),
  CONSTRAINT "fk_auteur_has_citation_citation1"
    FOREIGN KEY("citation_id")
    REFERENCES "citation"("id")
);
CREATE INDEX "mydb"."auteur_has_citation.fk_auteur_has_citation_citation1_idx" ON "auteur_has_citation" ("citation_id");
CREATE INDEX "mydb"."auteur_has_citation.fk_auteur_has_citation_auteur_idx" ON "auteur_has_citation" ("auteur_id");
COMMIT;
