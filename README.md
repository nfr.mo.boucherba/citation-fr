# citation-fr

citation.fr

## Contexte du projet
Le site citation.fr vient de se faire hacker. Notre client nous demande de faire une refonte complète de ce site.

​

Le header du site contient le logo, le nom du site "Citation.fr" et un menu permettant de naviguer entre les différentes pages du site.

En version mobile, le menu doit s'ouvrir a l'aide d'un burger menu.

En mobile et destkop, le header doit être sticky (et rester apparant lors que l'on scroll).

Le footer contient le texte suivant : "©copyright 2022 citation.fr"

## Les pages demandées sont :

### Homepage:
listant la dernière citation et son auteur (au clic sur l'auteur, j'accède à la page des citations de l'auteur)
### Les citations :
listant toutes les citations avec une pagination (10 citations par page)
### Les auteurs: 
listant tous les auteurs ayant des citations.
### Les citation d'un auteur: 
listant toutes les citations pour un auteur donné

## Vous devez aussi développer un backoffice permettant de :

voir toutes les citations
ajouter une citation
modifier une citation
supprimer une citation
voir toutes les auteurs
ajouter un auteur
modifier un auteur
supprimer un auteur

### Et comme le client aime bien les animations, vous devez ajouter une animation pour faire apparaitre la dernière citation publiée.

## Modalités pédagogiques
Framework au choix. CSS from scratch (pas de framework CSS autorisé)

Travail individuel

3j

## Critères de performance
Le résultat final doit être responsive et correspondre à aux maquettes.
​

## Livrables
Un repo gitlab contenant le code source, le MCD de la base de données, un script permettant de créer la base de données et les maquettes. 
Et si possible, une version en ligne du site
